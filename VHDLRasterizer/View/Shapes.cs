﻿using System.ComponentModel;
using System.Collections.ObjectModel;

using VHDLRasterizer.Rendering;
using VHDLRasterizer.Graphic;

namespace VHDLRasterizer.View
{
    public class Shapes
    {
        #region Collection

        ObservableCollection<PrimitiveViewModel> graphics = new ObservableCollection<PrimitiveViewModel>();

        public ObservableCollection<PrimitiveViewModel> Graphics
        {
            get { return graphics; }
            set
            {
                graphics = value;
            }
        }

        private PrimitiveViewModel shape;

        public PrimitiveViewModel Shape 
        {
            get 
            {
                if (shape != null)
                {
                    if (shape.GetType() == typeof(LineViewModel))
                        return (LineViewModel)shape;
                }
                return null;
            }
            set
            {
                shape = value;
            }
        }

        #endregion

        /// <summary>
        /// View-Model for creating new line.
        /// </summary>
        public ViewModelNewLine NewLine { get; private set; }


        public Shapes()
        {
            RemoveCommand = new Command.RemoveCommand(RemoveAt);
            ExportCommand = new Command.NewLineCommand(Export);
            NewLine = new ViewModelNewLine(Add);
        }


        #region Methods

        public void Add(Graphic.PrimitiveViewModel primitive)
        {
            Graphics.Add(primitive);
            primitive.PropertyChanged += new PropertyChangedEventHandler(Render);
            Output.Render(makeShapesTree());
        }

        void RemoveAt(int index)
        {
            Graphics.RemoveAt(index);
            Shape = null;
            Output.Render(makeShapesTree());
        }

        void Export()
        {
            var win = new Microsoft.Win32.SaveFileDialog();
            win.AddExtension = true;
            win.DefaultExt = ".txt";
            if (win.ShowDialog() == true)
                new IO.VHDL(Output.render).ExportToFile(win.FileName);
        }

        #endregion

        #region Rendering

        private void Render(object sender, PropertyChangedEventArgs e)
        {
            Output.Render(makeShapesTree());
        }

        private System.Collections.Generic.ICollection<Graphic.Primitive> makeShapesTree()
        {
            var collection = new System.Collections.Generic.List<Graphic.Primitive>();

            foreach (Graphic.PrimitiveViewModel p in Graphics)
                collection.Add(p.Shape);

            return collection;
        }

        #endregion //Rendering

        #region Commands

        public Command.NewLineCommand ExportCommand { get; set; }

        public Command.RemoveCommand RemoveCommand { get; set; }

        #endregion //Commands
    }

}
