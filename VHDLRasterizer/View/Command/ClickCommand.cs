﻿using System;
using System.Windows.Input;

namespace VHDLRasterizer.View.Command
{
    public class ClickCommand<T> : ICommand
    {
        readonly Action<T> execute;

        public ClickCommand(Action<T> execute)
        {
            this.execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event System.EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            execute((T)parameter);
        }
    }
}
