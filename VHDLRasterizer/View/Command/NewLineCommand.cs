﻿using System.Windows.Input;

namespace VHDLRasterizer.View.Command
{
    public class NewLineCommand : ICommand
    {
        readonly System.Action execute;

        public NewLineCommand(System.Action execute)
        {
            this.execute = execute;
        }


        #region interfaceMembers

        public event System.EventHandler CanExecuteChanged;

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            Execute();
        }

        #endregion


        public bool CanExecute()
        {
            return true;
        }

        public void Execute()
        {
            execute();
        }
    }
}
