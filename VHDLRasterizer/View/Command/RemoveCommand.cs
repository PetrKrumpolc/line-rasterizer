﻿using System;
using System.Windows.Input;

namespace VHDLRasterizer.View.Command
{
    public class RemoveCommand : ICommand
    {
        Action<int> execute;

        public RemoveCommand(Action<int> execute)
        {
            this.execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            if (parameter != null)
            {
                int index = (int)parameter;
                if (index > -1)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            execute((int)parameter);
        }
    }
}
