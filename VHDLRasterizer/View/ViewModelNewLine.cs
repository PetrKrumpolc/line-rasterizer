﻿using System;
using System.Linq;
using System.Reflection;

namespace VHDLRasterizer.View
{
    public class ViewModelNewLine : System.ComponentModel.IDataErrorInfo
    {
        Action<Graphic.LineViewModel> action;


        public string StartX { get; set; }

        public string StartY { get; set; }

        public string EndX { get; set; }

        public string EndY { get; set; }



        public ViewModelNewLine(Action<Graphic.LineViewModel> action)
        {
            this.action = action;
            NewLineCommand = new Command.NewLineCommand(AddLine);

            StartX = "0";
            StartY = "0";
            EndX = "0";
            EndY = "0";
        }

        void AddLine()
        {
            if (!existsErrors())
                action(new Graphic.LineViewModel(new Graphic.Line(255, 0, 0, new Tuple<int, int>(int.Parse(StartX), int.Parse(StartY)), new Tuple<int, int>(int.Parse(EndX), int.Parse(EndY)))));     
        }

        public Command.NewLineCommand NewLineCommand { get; set; }


        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get 
            {
                int integer;
                PropertyInfo pI = typeof(ViewModelNewLine).GetProperty(columnName);   
    
                if(int.TryParse(pI.GetValue(this).ToString(), out integer))
                {
                    if (integer < 0)
                        return "Souřadnice musí být > -1";
                    else if ((columnName == "StartX" || columnName == "EndX") && integer > 799)
                        return "Souřadnice musí být < 800";
                    else if ((columnName == "StartY" || columnName == "EndY") && integer > 599)
                        return "Souřadnice musí být < 600";
                }
                else
                    return "Vložená hodnota obsahuje neplatné znaky.";

                return null; 
            }
        }

        private bool existsErrors()
        {
            PropertyInfo[] info = typeof(ViewModelNewLine).GetProperties();
            for(byte i = 0;i < info.Length - 3;i++)
            {
                if (this[info[i].Name] != null)
                    return true;
            }
            return false;
        }
    }
}
