﻿using System;
using System.Reflection;

namespace VHDLRasterizer.Graphic
{
    public class LineViewModel : PrimitiveViewModel
    {

        public LineViewModel(Graphic.Primitive shape) 
            : base(shape)
        {
            Line line = (Line)shape;
            StartX = line.StartPoint.Item1.ToString();
            StartY = line.StartPoint.Item2.ToString();
            EndX = line.EndPoint.Item1.ToString();
            EndY = line.EndPoint.Item2.ToString();
        }


        public string StartX { get; set; }

        public string StartY { get; set; }

        public string EndX { get; set; }

        public string EndY { get; set; }

        #region ErrorProvider

        public override string Error
        {
            get { return null; }
        }

        public override string this[string columnName]
        {
            get
            {
                int integer;
                PropertyInfo pI = typeof(LineViewModel).GetProperty(columnName);
                if (int.TryParse(pI.GetValue(this).ToString(), out integer))
                {
                    if (integer < 0)
                        return "Souřadnice musí být > -1";
                    else if ((columnName == "StartX" || columnName == "EndX") && integer > 799)
                        return "Souřadnice musí být < 800";
                    else if ((columnName == "StartY" || columnName == "EndY") && integer > 599)
                        return "Souřadnice musí být < 600";
                }
                else
                    return "Vložená hodnota obsahuje neplatné znaky.";

                switch (columnName)
                {
                    case "StartX":
                            ((Line)Shape).StartPoint = new System.Tuple<int, int>(int.Parse(StartX), ((Line)Shape).StartPoint.Item2); 
                        break;
                    case "StartY":
                            ((Line)Shape).StartPoint = new System.Tuple<int, int>(((Line)Shape).StartPoint.Item1, int.Parse(StartY));
                        break;
                    case "EndX":
                            ((Line)Shape).EndPoint = new System.Tuple<int, int>(int.Parse(EndX), ((Line)Shape).EndPoint.Item2);
                        break;
                    case "EndY":
                            ((Line)Shape).EndPoint = new System.Tuple<int, int>(((Line)Shape).EndPoint.Item1, int.Parse(EndY));
                        break;
                }
                return null;
            }
        }

        #endregion
    }
}
