﻿namespace VHDLRasterizer.Graphic
{
    public class Pixel
    {
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }

        public int X { get; set; }
        public int Y { get; set; }

        public override string ToString()
        {
            return string.Concat("R:",R.ToString(), " G:", G.ToString(), " B:",B.ToString(), " | Point");
        }
    }
}
