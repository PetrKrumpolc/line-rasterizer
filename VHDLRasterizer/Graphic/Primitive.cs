﻿using System.ComponentModel;

namespace VHDLRasterizer.Graphic
{
    public abstract class Primitive : INotifyPropertyChanged
    {
        private byte r;
        public byte R 
        {
            get { return r; }
            set 
            { 
                r = value;
                OnPropertyChanged("R");
            } 
        }

        private byte g;
        public byte G
        {
            get { return g; }
            set
            {
                g = value;
                OnPropertyChanged("G");
            }
        }

        private byte b;
        public byte B
        {
            get { return b; }
            set
            {
                b = value;
                OnPropertyChanged("B");
            }
        }

        public Primitive(byte R, byte G, byte B)
        {
            this.R = R;
            this.G = G;
            this.B = B;
        }

        public abstract void Render(Rendering.SetPixelDelegate setPixel);

        #region OverrideMembers

        public override string ToString()
        {
            return string.Concat(" R:", R.ToString(), " G:", G.ToString(), " B:", B.ToString());
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
