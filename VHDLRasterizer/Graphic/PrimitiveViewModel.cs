﻿using System.ComponentModel;

namespace VHDLRasterizer.Graphic
{
    public abstract class PrimitiveViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        public Graphic.Primitive Shape { get; private set; }

        public string[] GetColors
        {
            get { return new string[] { "Červená", "Zelená", "Modrá", "Žlutá", "Cyan", "Magenta", "Bílá" }; }
        }

        public string Color
        {
            get
            {
                if (Shape != null)
                {
                    if (Shape.R == 255 && Shape.G == 0 && Shape.B == 0)
                        return "Červená";
                    else if (Shape.R == 255 && Shape.G == 255 && Shape.B == 0)
                        return "Žlutá";
                    else if (Shape.R == 0 && Shape.G == 255 && Shape.B == 0)
                        return "Zelená";
                    else if (Shape.R == 0 && Shape.G == 0 && Shape.B == 255)
                        return "Modrá";
                    else if (Shape.R == 255 && Shape.G == 0 && Shape.B == 255)
                        return "Magenta";
                    else if (Shape.R == 0 && Shape.G == 255 && Shape.B == 255)
                        return "Cyan";
                    else
                        return "Bílá";
                }
                return "";
            }
            set
            {
                switch (value)
                {
                    case "Červená":
                        Shape.R = 255;
                        Shape.G = 0;
                        Shape.B = 0;
                        break;
                    case "Zelená":
                        Shape.R = 0;
                        Shape.G = 255;
                        Shape.B = 0;
                        break;
                    case "Modrá":
                        Shape.R = 0;
                        Shape.G = 0;
                        Shape.B = 255;
                        break;
                    case "Žlutá":
                        Shape.R = 255;
                        Shape.G = 255;
                        Shape.B = 0;
                        break;
                    case "Magenta":
                        Shape.R = 255;
                        Shape.G = 0;
                        Shape.B = 255;
                        break;
                    case "Cyan":
                        Shape.R = 0;
                        Shape.G = 255;
                        Shape.B = 255;
                        break;
                    default:
                        Shape.R = 255;
                        Shape.G = 255;
                        Shape.B = 255;
                        break;
                }
            }

        }


        public PrimitiveViewModel(Graphic.Primitive shape)
        {
            Shape = shape;
            Shape.PropertyChanged += new PropertyChangedEventHandler(OnPropertyChange);
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChange(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(sender, new PropertyChangedEventArgs(e.PropertyName));
        }

        #endregion

        #region IDataErrorInfo

        public abstract string Error { get; }

        public abstract string this[string columnName] { get; }

        #endregion

        #region OverrideMembers

        public override string ToString()
        {
            return Shape.ToString();
        }

        #endregion
    }
}
