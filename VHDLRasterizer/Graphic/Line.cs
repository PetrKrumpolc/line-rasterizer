﻿using System;

namespace VHDLRasterizer.Graphic
{
    public class Line : Primitive
    {
        Tuple<int, int> startPoint = new Tuple<int, int>(0, 0);

        public Tuple<int, int> StartPoint
        {
            get { return startPoint; }
            set
            {
                startPoint = value;
                OnPropertyChanged("StartPoint");
            }
        }


        Tuple<int, int> endPoint = new Tuple<int, int>(0, 0);

        public Tuple<int, int> EndPoint
        {
            get { return endPoint; }
            set
            {
                endPoint = value;
                OnPropertyChanged("EndPoint");
            }
        }


        public Line(byte R, byte G, byte B, Tuple<int,int> StartPoint, Tuple<int,int> EndPoint)
            :base(R,G,B)
        {
            startPoint = StartPoint;
            endPoint = EndPoint;
        }


        public override void Render(Rendering.SetPixelDelegate setPixel)
        {
            new VHDLRasterizer.Rendering.Rasterisation.Bresenham().Line(this, setPixel);
        }

        #region OverrideMembers

        public override string ToString()
        {
            return "Line";
        }

        #endregion
    }
}
