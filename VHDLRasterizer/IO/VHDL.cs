﻿using System;
using System.IO;

namespace VHDLRasterizer.IO
{
    public class VHDL
    {
        //VHDL syntax
        //R <= '1' WHEN (count_h > "0000000000" and count_h < "100101011") ELSE '0';

        int count_h = 0;
        int count_v = 0;

        byte[] data;
        byte bitsPerPixel;

        public VHDL(System.Windows.Media.Imaging.WriteableBitmap output)
        {
            bitsPerPixel = (byte)(output.Format.BitsPerPixel / 8);
            data = new byte[(output.PixelWidth * output.PixelHeight) * bitsPerPixel];
            output.CopyPixels(data, output.PixelWidth * bitsPerPixel, 0);
        }

        public void ExportToFile(string path)
        {
            if(Directory.Exists(Path.GetDirectoryName(path)))
            {
                using(StreamWriter sw = new StreamWriter(path))
                {
                    bool _continue;
                    bool firstWritted;
                    sw.WriteLine("-- Generated code by VHDL Rasterizer, Copyright 2015, Petr Krumpolc --");

                    for (byte b = 0; b < 3; b++)
                    {
                        _continue = false;

                        for (int j = 0; j < data.Length; j += bitsPerPixel)
                        {
                            if (data[j + b] == 255)
                            {
                                if(b == 0)
                                    sw.WriteLine("R <= '1' WHEN (");
                                else if (b == 1)
                                    sw.WriteLine("G <= '1' WHEN (");
                                else
                                    sw.WriteLine("B <= '1' WHEN (");
                                _continue = true;
                                break;
                            }
                        }
                       
                        if (!_continue)
                            continue;

                        count_h = 0;
                        count_v = 0;
                        firstWritted = false;

                        for (int i = 0; i < data.Length; i += bitsPerPixel)
                        {
                            if (data[i + b] != 0)
                            {
                                if (firstWritted)
                                    sw.WriteLine("or");
                                sw.Write(getCondition());
                                if (!firstWritted)
                                    firstWritted = true;
                            }

                            if (count_h == 799 && count_v == 599)
                                sw.WriteLine(") ELSE '0';");

                            if (count_h < 799)
                                count_h++;
                            else
                            {
                                count_h = 0;
                                count_v++;
                            }
                        }
                    }
                    sw.WriteLine();
                }
            }
        }

        string getCondition()
        {
           return  "\t\t( count_h = \"" + Convert.ToString(count_h, 2) + "\" and count_v = \"" + Convert.ToString(count_v, 2) + "\" )";
        }
    }
}
