﻿using System;
using VHDLRasterizer.Graphic;

namespace VHDLRasterizer.Rendering.Rasterisation
{
    class Bresenham
    {
        public void Line(Line line, SetPixelDelegate setPixel)
        {
            if (line.StartPoint.Item1 == line.EndPoint.Item1 && line.StartPoint.Item2 == line.EndPoint.Item2)
            {
                setPixel(new Graphic.Pixel()
                {
                    R = line.R,
                    G = line.G,
                    B = line.B,
                    X = line.StartPoint.Item1,
                    Y = line.StartPoint.Item2
                });
            }
            else
            {
                int X1 = line.StartPoint.Item1;
                int Y1 = line.StartPoint.Item2;

                int dx = Math.Abs(line.EndPoint.Item1 - line.StartPoint.Item1);
                int dy = Math.Abs(line.EndPoint.Item2 - line.StartPoint.Item2);
                int difference = dx - dy;

                int shiftX, shiftY, p;

                if (line.StartPoint.Item1 < line.EndPoint.Item1)
                    shiftX = 1;
                else
                    shiftX = -1;

                if (line.StartPoint.Item2 < line.EndPoint.Item2)
                    shiftY = 1;
                else
                    shiftY = -1;

                Graphic.Pixel pix = new Pixel();

                while(X1 != line.EndPoint.Item1 || Y1 != line.EndPoint.Item2)
                {

                    pix.R = line.R;
                    pix.G = line.G;
                    pix.B = line.B;
                    pix.X = X1;
                    pix.Y = Y1;
                    setPixel(pix);

                    p = 2 * difference;

                    if(p > -dy)
                    {
                        difference -= dy;
                        X1 += shiftX;
                    }

                    if(p < dx)
                    {
                        difference += dx;
                        Y1 += shiftY;
                    }
                }
            }
        }
    }
}
