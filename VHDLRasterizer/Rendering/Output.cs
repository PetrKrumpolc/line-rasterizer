﻿using System.Collections;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace VHDLRasterizer.Rendering
{
    public static class Output
    {
        static WriteableBitmap wBMP = new WriteableBitmap(800, 600, 72, 72, System.Windows.Media.PixelFormats.Rgb24, null);

        static byte[] data;

        public static WriteableBitmap render 
        { 
            get { return wBMP; }
        }


        static Output()
        {
            data = new byte[(render.PixelWidth * render.PixelHeight) * (render.Format.BitsPerPixel / 8)]; 
        }


        public static void Render(System.Collections.Generic.ICollection<Graphic.Primitive> graphics)
        {
            ResetPixels();
            foreach(Graphic.Primitive p in graphics)
                p.Render(SetPixel);

            render.WritePixels(new System.Windows.Int32Rect(0, 0, render.PixelWidth, render.PixelHeight), data, render.PixelWidth * 3, 0);
        }

        private static void ResetPixels()
        {
            for (int i = 0; i < data.Length; i++)
                data[i] = 0;
        }

        private static void SetPixel(Graphic.Pixel pixel)
        {
            int index = ((pixel.Y * render.PixelWidth) + pixel.X) * 3;
            if (index > -1 && index < ((render.PixelWidth * render.PixelHeight) * (render.Format.BitsPerPixel / 8)))
            {
                data[index] = pixel.R;
                data[index + 1] = pixel.G;
                data[index + 2] = pixel.B;
            }
            else
                throw new System.IndexOutOfRangeException("Coordinates are out of range your bitmap.");
        }

    }
}
